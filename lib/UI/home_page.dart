import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:bloc/bloc.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:mascotas/authentication/bloc/authentication_bloc.dart';
import 'package:mascotas/navigation/bloc/navigation_bloc.dart';
import 'package:flutter_cache_manager/flutter_cache_manager.dart';
import '../main.dart';
import 'home_page.dart';

class HomePage extends StatefulWidget {
  const HomePage({Key? key}) : super(key: key);

  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  static final customCacheManager = CacheManager(Config(
    'customKey',
    stalePeriod: Duration(days: 69),
  ));
  //--------------Fuciones--------------------

  Widget giveImage() {
    return Container(
      child: CircleAvatar(
        backgroundImage: CachedNetworkImageProvider(
          'https://source.unsplash.com/random?sig=1',
        ),
      ),
    );
  }

  Widget buildImage2(int index) => CircleAvatar(
        child: CachedNetworkImage(
          imageUrl: 'https://source.unsplash.com/random?sig=1',
        ),
      );

  Widget buildImage3(int index) => ClipRRect(
        borderRadius: BorderRadius.circular(12),
        child: CachedNetworkImage(
          key: UniqueKey(),
          imageUrl: 'https://source.unsplash.com/random?sig=$index',
          height: 50,
          width: 50,
          fit: BoxFit.cover,
          maxHeightDiskCache: 75,
          placeholder: (context, url) => Image.asset('assets/images/michi.PNG'),
          errorWidget: (context, url, error) => Container(
            color: Colors.black12,
            child: Icon(Icons.error, color: Colors.redAccent),
          ),
        ),
      );

  Widget buildImage(int index) => ClipRRect(
        borderRadius: BorderRadius.circular(12),
        child: CachedNetworkImage(
          cacheManager: customCacheManager,
          key: UniqueKey(),
          imageUrl: 'https://source.unsplash.com/random?sig=$index',
          height: 50,
          width: 50,
          fit: BoxFit.cover,
          maxHeightDiskCache: 75,
          placeholder: (context, url) => Image.asset('assets/images/michi.PNG'),
          errorWidget: (context, url, error) => Container(
            color: Colors.black12,
            child: Icon(Icons.error, color: Colors.redAccent),
          ),
        ),
      );

  void limpiarCache() {
    DefaultCacheManager().emptyCache();
    imageCache!.clear();
    imageCache!.clearLiveImages();
    setState(() {});
  }

  //-----------------Fin Funciones--------------

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        actions: [
          TextButton(
            style: TextButton.styleFrom(primary: Colors.white),
            child: Text("Limpiar Cache"),
            onPressed: limpiarCache,
          ),
        ],
        title: Text('Animales en Adopción'),
        backgroundColor: Color.fromRGBO(8, 7, 8, 1),
        centerTitle: true,
      ),
      body: ListView.builder(
        padding: EdgeInsets.all(8),
        itemCount: 10,
        itemBuilder: (context, index) => Card(
          color: Colors.white,
          child: ListTile(
            contentPadding: EdgeInsets.symmetric(horizontal: 16, vertical: 16),
            leading: buildImage3(index), //buildImage(index),
            title: Text(
              'Mascota ${index + 1}',
              style: TextStyle(color: Colors.black),
            ),
          ),
        ),
      ),
      bottomNavigationBar: BottomNavigationBar(
        items: const <BottomNavigationBarItem>[
          BottomNavigationBarItem(
            icon: Icon(Icons.home),
            label: '',
            backgroundColor: Color.fromRGBO(8, 7, 8, 1),
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.search),
            label: '',
            backgroundColor: Color.fromRGBO(8, 7, 8, 1),
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.chat_bubble),
            label: '',
            backgroundColor: Color.fromRGBO(8, 7, 8, 1),
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.person),
            label: '',
            backgroundColor: Color.fromRGBO(8, 7, 8, 1),
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.exit_to_app),
            label: 'logout',
            backgroundColor: Color.fromRGBO(8, 7, 8, 1),
          ),
        ],
        onTap: (value) {
          if (value == 4) context.read<AuthenticationBloc>().add(AuthenticationSignedOut());
        },
      ),
    );
  }
}

/// This is the stateless widget that the main application instantiates.
class CardType1 extends StatelessWidget {
  const CardType1({Key? key}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Center(
      child: Card(
        child: InkWell(
          splashColor: Colors.blue.withAlpha(30),
          onTap: () {
            print('animal buscado');
          },
          child: const SizedBox(
            width: 350,
            height: 100,
            child: Text('animal buscado'),
          ),
        ),
      ),
    );
  }
}

class CardType2 extends StatelessWidget {
  const CardType2({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Center(
      child: Card(
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: <Widget>[
            const ListTile(
              leading: Icon(Icons.pets),
              title: Text('Especie de la Mascota: Perro normal'),
              subtitle: Text('Nombre: Chancleto'),
            ),
            Row(
              children: <Widget>[
                Align(
                  alignment: Alignment.center,
                  child: Container(
                    child: Image.asset(
                      'assets/images/michi.PNG',
                      height: 200,
                      fit: BoxFit.cover,
                    ),
                  ),
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }
}

// hello
