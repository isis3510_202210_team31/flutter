import 'package:bloc/bloc.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:meta/meta.dart';

part 'navigation_event.dart';
part 'navigation_state.dart';

class NavigationBloc extends Bloc<NavigationEvent, NavigationState> {
  NavigationBloc() : super(StateLogin()) {
    on<EventLogin>((event, emit) => emit(StateLogin()));
    on<EventHome>((event, emit) => emit(StateHome()));
    on<EventRegister>((event, emit) => emit(StateRegister()));
  }
}
