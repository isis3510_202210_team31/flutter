part of 'navigation_bloc.dart';

@immutable
abstract class NavigationEvent {}

class EventLogin extends NavigationEvent{}

class EventHome extends NavigationEvent{}

class EventRegister extends NavigationEvent{}