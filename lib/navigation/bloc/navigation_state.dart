part of 'navigation_bloc.dart';

@immutable
abstract class NavigationState {}

class StateLogin extends NavigationState {}

class StateHome extends NavigationState {}

class StateRegister extends NavigationState {}
