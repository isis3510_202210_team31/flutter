import 'package:flutter/material.dart';

//only valid for one statement
void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  // the build method has to return a widget
  // using types helps for writing clearer and better code.
  Widget build(BuildContext context) {
    //material app widget that encapsulates the entire app. Widgets return widgets. The wrapper for the entire app
    // material takes named arguments: name:(value of the argument) ,
    // scaffold creates a new page in the app with the ability to add new widgets. Scaffold needs arguments
    // home: widget drawn into the screen when the screen loads

    return MaterialApp(
      home: Scaffold(
        appBar: AppBar(
          title: Text('Animales en Adopción'),
          backgroundColor: Color.fromRGBO(8, 7, 8, 1),
          centerTitle: true,
        ),
        body: const CardType2(),
        bottomNavigationBar: BottomNavigationBar(
          items: const <BottomNavigationBarItem>[
            BottomNavigationBarItem(
              icon: Icon(Icons.home),
              label: '',
              backgroundColor: Color.fromRGBO(8, 7, 8, 1),
            ),
            BottomNavigationBarItem(
              icon: Icon(Icons.search),
              label: '',
              backgroundColor: Color.fromRGBO(8, 7, 8, 1),
            ),
            BottomNavigationBarItem(
              icon: Icon(Icons.chat_bubble),
              label: '',
              backgroundColor: Color.fromRGBO(8, 7, 8, 1),
            ),
            BottomNavigationBarItem(
              icon: Icon(Icons.person),
              label: '',
              backgroundColor: Color.fromRGBO(8, 7, 8, 1),
            ),
          ],
        ),
      ),
    );
  }
}

/// This is the stateless widget that the main application instantiates.
class CardType1 extends StatelessWidget {
  const CardType1({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Center(
      child: Card(
        child: InkWell(
          splashColor: Colors.blue.withAlpha(30),
          onTap: () {
            print('animal buscado');
          },
          child: const SizedBox(
            width: 350,
            height: 100,
            child: Text('animal buscado'),
          ),
        ),
      ),
    );
  }
}

class CardType2 extends StatelessWidget {
  const CardType2({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Center(
      child: Card(
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: <Widget>[
            const ListTile(
              leading: Icon(Icons.pets),
              title: Text('Especie de la Mascota: Perro normal'),
              subtitle: Text('Nombre: Chancleto'),
            ),
            Row(
              children: <Widget>[
                Align(
                  alignment: Alignment.center,
                child: Container(
                  child: Image.asset(
                    'assets/images/michi.PNG',
                    height: 200,
                    fit: BoxFit.cover,
                  ),
                ),
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }
}

// hello 