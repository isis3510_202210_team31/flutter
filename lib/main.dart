import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:mascotas/UI/login.dart';
import 'package:mascotas/UI/signup.dart';
import 'package:mascotas/app_bloc_observer.dart';
import 'package:mascotas/authentication/authentication_repository.dart';
import 'package:mascotas/database/bloc/database_bloc.dart';
import 'package:mascotas/database/database_repository_impl.dart';
import 'package:mascotas/form-validation/bloc/form_bloc.dart';
import 'package:mascotas/navigation/bloc/navigation_bloc.dart';
import 'package:mascotas/authentication/bloc/authentication_bloc.dart';
import 'package:mascotas/UI/home_page.dart';


void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await Firebase.initializeApp();
  BlocOverrides.runZoned(
    () => runApp(MultiBlocProvider(
      providers: [
        BlocProvider(
          create: (context) =>
              NavigationBloc(),
        ),
        BlocProvider(
          create: (context) =>
              AuthenticationBloc(AuthenticationRepositoryImpl())
                ..add(AuthenticationStarted()),
        ),
        BlocProvider(
          create: (context) => FormBloc(
              AuthenticationRepositoryImpl(), DatabaseRepositoryImpl()),
        ),
        BlocProvider(
          create: (context) => DatabaseBloc(DatabaseRepositoryImpl()),
        )
      ],
      child: const MyApp(),
    )),
    blocObserver: AppBlocObserver(),
  );
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Patitas de Paz',
      theme: ThemeData(
        primarySwatch: swatchColor,
      ),
//      routes: {
//        '/': (context) => Login(),
//        '/home': (context) => HomePage(),
//      },
//      initialRoute: '/',
      home: BlocBuilder<NavigationBloc, NavigationState>(
        builder: (context, stateNav) {
          return BlocBuilder<AuthenticationBloc, AuthenticationState>(
            builder: (context, stateAuth){
              if (stateAuth is AuthenticationSuccess) {
                return const HomePage();
              }
              else if(stateNav is StateRegister){
                return const SignUp();
              }
              else{
                return const Login();
              }
            }
          );
        },
      ),
    );
  }
}

const MaterialColor swatchColor = MaterialColor(
  _swatchColor,
  <int, Color>{
    50: Color(0xFFAA4465),
    100: Color(0xFFAA4465),
    200: Color(0xFFAA4465),
    300: Color(0xFFAA4465),
    400: Color(0xFFAA4465),
    500: Color(_swatchColor),
    600: Color(0xFFAA4465),
    700: Color(0xFFAA4465),
    800: Color(0xFFAA4465),
    900: Color(0xFFAA4465),
  },
);
const int _swatchColor = 0xFFAA4465;
